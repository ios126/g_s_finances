//
//  GanhosViewModel.swift
//  G`s Finances
//
//  Created by ALUNO on 11/05/22.
//

import Foundation

class GastosViewModel:ObservableObject{
    @Published var nome:String = ""
    var gasto:Double = 0.0
    var sqlite = SQLiteDatabase()
    var gastosBoard:[GastosModel]
    var tipo:String = ""
    var totalGastos: Double = 0.0
    
    init(){
        gastosBoard = sqlite.getGastos()
        getGastosTotal()
    }
    
    func getGastosTotal(){
        totalGastos = 0.0
        var a = sqlite.getGastos()
        a.forEach{ objeto in
            totalGastos = totalGastos + objeto.gasto
        }
        print("Total:", totalGastos)
    }
    
    func insertNewGasto(gasto:GastosModel){
        _ = sqlite.insertGastos(gastos: gasto)
        gastosBoard = sqlite.getGastos()
        getGastosTotal()
    }
}
