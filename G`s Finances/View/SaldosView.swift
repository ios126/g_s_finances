//
//  SaldosView.swift
//  G`s Finances
//
//  Created by ALUNO on 04/05/22.
//

import SwiftUI
import SunburstDiagram

struct SaldosView: View {
    @EnvironmentObject var ganhosDataModel:GanhosViewModel
    @EnvironmentObject var gastosDataModel:GastosViewModel
    
    
    var body: some View {
        VStack(alignment: .center){
            VStack{
            Text("Saldo:").multilineTextAlignment(.center).font(.title)
            Text("R$ \(ganhosDataModel.totalGanhos - gastosDataModel.totalGastos, specifier: "%.2f")").font(.title)
            }
            
            
            // Create your configuration model
            let configuration = SunburstConfiguration(nodes: [
                Node(name: "Gasto", value: gastosDataModel.totalGastos, backgroundColor: .systemOrange),
                Node(name: "Ganho", value: ganhosDataModel.totalGanhos, backgroundColor: .systemMint)
            ], calculationMode: .parentDependent(totalValue: nil))

            // Get the view controller for the SunburstView
            SunburstView(configuration: configuration).foregroundColor(.white)
            
            
            VStack(alignment: .leading) {
            HStack{
                Text("Total de gastos: ")
                Text("R$ \(gastosDataModel.totalGastos, specifier: "%.2f")")
            }
            HStack{
                Text("Total de ganhos: ")
                Text("R$ \(ganhosDataModel.totalGanhos, specifier: "%.2f")")
            }
            }.padding(.trailing, 20.0)
            Spacer()
        }
    
        
        
    }
}

struct SaldosView_Previews: PreviewProvider {
    static var previews: some View {
        SaldosView()
    }
}
