//
//  GanhosViewModel.swift
//  G`s Finances
//
//  Created by ALUNO on 11/05/22.
//

import Foundation

class GanhosViewModel:ObservableObject{
    @Published var nome:String = ""
    var ganho:Double = 0.0
    var sqlite = SQLiteDatabase()
    var ganhosBoard:[GanhosModel]
    var totalGanhos: Double = 0.0
    
    init(){
        ganhosBoard = sqlite.getGanhos()
        getGanhosTotal()
    }
    
    func getGanhosTotal(){
        totalGanhos = 0.0
        var a = sqlite.getGanhos()
        a.forEach{ objeto in
            totalGanhos = totalGanhos + objeto.ganho
        }
        print("Total:", totalGanhos)
    }
    func insertNewGanho(ganho:GanhosModel){
        _ = sqlite.insertGanhos(ganhos: ganho)
        ganhosBoard = sqlite.getGanhos()
        getGanhosTotal()
    }
}
