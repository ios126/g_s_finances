//
//  GastosView.swift
//  G`s Finances
//
//  Created by ALUNO on 04/05/22.
//

import SwiftUI

struct GastosView: View {
    @EnvironmentObject var gastosDataModel:GastosViewModel
    @State private var string = ""
    @State private var string2 = ""
    
    var body: some View {
        VStack{
            Text("Gastos").font(.title)
            Spacer()
            TextField("Insira com o que gastou:", text:$string2)
            HStack{
                Text("Valor: R$         ")
                TextField("0,00", text:$string)
                Button(action:{
                    gastosDataModel.gasto = Double(string)!
                    gastosDataModel.tipo = string2
                    self.gastosDataModel.insertNewGasto(gasto: GastosModel(gasto: self.gastosDataModel.gasto, tipo: self.gastosDataModel.tipo))
                    string = ""
                    string2 = ""
                    
                    
                }){
                    Text("adicionar")
                }
                Spacer()
            }.padding(20)
            Spacer()
            Spacer()
            Spacer()
            List{
                ForEach (gastosDataModel.gastosBoard.indices , id: \.self){i in GastoItemRow(position: i, gastoModel: gastosDataModel.gastosBoard[i])}
            }
        }.padding(10)
}
}

struct GastosView_Previews: PreviewProvider {
    static var previews: some View {
        GastosView().environmentObject(GastosViewModel())
    }
}
