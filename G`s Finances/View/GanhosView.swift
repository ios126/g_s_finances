//
//  GanhosView.swift
//  G`s Finances
//
//  Created by ALUNO on 04/05/22.
//

import SwiftUI

struct GanhosView: View {
    @EnvironmentObject var ganhosDataModel:GanhosViewModel
    @State private var string = ""

    var body: some View {
        VStack{
            Text("Ganhos").font(.title)
            Spacer()
            Text("Insira o valor do seu novo ganho:")
            HStack{
                Text("R$ ")
                TextField("0,00", text:$string)
                
                Button(action:{
                    ganhosDataModel.ganho = Double(string)!
                    self.ganhosDataModel.insertNewGanho(ganho: GanhosModel(ganho: self.ganhosDataModel.ganho))
                    string = ""
                    
                }){
                    Text("adicionar")
                }
                Spacer()
            }.padding(20)
            Spacer()
            Spacer()
            Spacer()
            List{
                ForEach (ganhosDataModel.ganhosBoard.indices , id: \.self){i in GanhoItemRow(position: i, ganhoModel: ganhosDataModel.ganhosBoard[i])}
            }
        }.padding(10)
    }
}

struct GanhosView_Previews: PreviewProvider {
    static var previews: some View {
        GanhosView()
    }
}
    
