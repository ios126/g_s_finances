//
//  MenuView.swift
//  G`s Finances
//
//  Created by ALUNO on 04/05/22.
//

import SwiftUI

struct MenuView: View {
    @State private var isShowingSaldosView = false
    @State private var isShowingGanhosView = false
    @State private var isShowingGastosView = false
    var body: some View {
        NavigationView{
            VStack{
                Text("Menu")
                    .font(.title)
                Spacer()
                NavigationLink(destination: SaldosView(), isActive: $isShowingSaldosView){
                    Button("Saldos"){
                        isShowingSaldosView = true
                    }.buttonStyle(GrowingButton())
                }
                .isDetailLink(false)
                Spacer()
                NavigationLink(destination: GanhosView(), isActive: $isShowingGanhosView){
                    Button("Ganhos"){
                        isShowingGanhosView = true
                    }.buttonStyle(GrowingButton())
                }
                .isDetailLink(false)
                Spacer()
                NavigationLink(destination: GastosView(), isActive: $isShowingGastosView){
                    Button("Gastos"){
                        isShowingGastosView = true
                    }.buttonStyle(GrowingButton())
                }
                .isDetailLink(false)
                Spacer()
            }
        }.navigationBarHidden(true)
            .navigationBarTitleDisplayMode(.inline)
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}

