//
//  ContentView.swift
//  G`s Finances
//
//  Created by ALUNO on 04/05/22.
//

import SwiftUI

struct ContentView: View {
    @State private var isShowingMenuView = false
    @EnvironmentObject var dataModel:ContentViewModel
    var body: some View { 
        NavigationView{
            VStack{
                Text("G's Finances")
                    .font(.title)
                Spacer()
                HStack(alignment: .center){
                    Text("Insira seu Username:")
                    TextField("Nome", text:$dataModel.nome)
                }
                .padding(.horizontal, 20.0)
                Spacer()
                NavigationLink(destination: MenuView(), isActive: $isShowingMenuView){
                    Button("Iniciar"){
                        isShowingMenuView = true
                    }.buttonStyle(GrowingButton())
                }
                .isDetailLink(false)
                Spacer()
            }.padding(20)
        }.navigationBarHidden(true)
            .navigationBarTitleDisplayMode(.inline)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        .environmentObject(ContentViewModel())
        
    }
}

struct GrowingButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(.mint)
            .foregroundColor(.white)
            .clipShape(Capsule())
            .scaleEffect(configuration.isPressed ? 1.2 : 1)
            .animation(.easeOut(duration: 0.2), value: configuration.isPressed)
    }
}
