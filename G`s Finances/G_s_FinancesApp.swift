//
//  G_s_FinancesApp.swift
//  G`s Finances
//
//  Created by ALUNO on 04/05/22.
//

import SwiftUI

@main
struct G_s_FinancesApp: App {
    @StateObject var dataModel = ContentViewModel()
    @StateObject var gastosDataModel = GastosViewModel()
    @StateObject var ganhosDataModel = GanhosViewModel()
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(dataModel).environmentObject(gastosDataModel).environmentObject(ganhosDataModel)
        }
    }
}
