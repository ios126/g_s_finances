//
//  GastoItemRow.swift
//  G`s Finances
//
//  Created by ALUNO on 18/05/22.
//

import SwiftUI

struct GastoItemRow: View {
    var position: Int
    var gastoModel:GastosModel
    var body: some View {
        HStack{
            Text("\(position)")
            Spacer()
            Text(gastoModel.tipo)
            Spacer()
            Text("R$ \(gastoModel.gasto, specifier: "%.2f")")
        }
    }
}

struct GastoItemRow_Previews: PreviewProvider {
    static var previews: some View {
        GastoItemRow(position: 1, gastoModel:GastosModel(gasto:0.0, tipo:"Cocada")).previewLayout(.sizeThatFits)
    }
}
