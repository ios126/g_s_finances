//
//  File.swift
//  G`s Finances
//
//  Created by ALUNO on 05/05/22.
//

import Foundation

import SQLite

class SQLiteDatabase{
    var db:Connection?
    
    let ganhosTable = Table("Ganhos")
    let gastosTable = Table("Gastos")
    
    let id = Expression<Int64>("id")
    let ganho = Expression<Double>("ganho")
    
    let gasto = Expression<Double>("gasto")
    let tipo = Expression<String>("tipo")
    
    
    init(){
        do{
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            db = try Connection("\(path)/gfinances1.sqlite3")
            try db?.run(ganhosTable.create(ifNotExists: true){
                t in
                t.column(id,primaryKey: .autoincrement)
                t.column(ganho)
                
                
            })
            try db?.run(gastosTable.create(ifNotExists: true){
                t in
                t.column(id,primaryKey: .autoincrement)
                t.column(gasto)
                t.column(tipo)
                
                
            })
        }catch{
            print(error)
        }
    }

    func insertGanhos(ganhos:GanhosModel) -> Bool{
        guard let db = db else{
            return false
        }
        do{
            let rowId = try db.run(ganhosTable.insert(
                ganho <- ganhos.ganho
            
            ))
            if rowId > 0{
                return true
            }
        }catch{
            print(error)
        }
        return false
    }
    
    func getGanhos() -> [GanhosModel]{
        var ganhos = [GanhosModel]()
        if let db = db{
            do{
                for row in try db.prepare(ganhosTable.select(ganho).order(id.desc)){
                    ganhos.append(GanhosModel( ganho: row[ganho]))
                }
            }catch{
                print(error)
            }
        }
        return ganhos
    }
    
    func insertGastos(gastos: GastosModel) -> Bool{
        guard let db = db else{
        return false
    }
    do{
        let rowId = try db.run(gastosTable.insert(
            gasto <- gastos.gasto,
            tipo <- gastos.tipo
        ))
        if rowId > 0{
            return true
        }
    }catch{
        print(error)
    }
    return false
    }
    
    
    func getGastos() -> [GastosModel] {
        var gastos = [GastosModel]()
        if let db = db{
            do{
                for row in try db.prepare(gastosTable.select(gasto,tipo).order(id.desc)){
                    gastos.append(GastosModel(gasto: row[gasto], tipo: row[tipo]))
                }
            }catch{
                print(error)
            }
        }
        return gastos
    }
     /*
    func getGastosTotal   () -> [GastosModel] {
        var gastos = [GastosModel]()
        if let db = db{
            do{
                for row in try db.prepare(gastosTable.sum(gasto)){
                    gastos.append(GastosModel(gasto: row[gasto], tipo: row[tipo]))
                }
            }catch{
                print(error)
            }
        }
        return gastos
    }*/
    
}
