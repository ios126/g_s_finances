//
//  GastosModel.swift
//  G`s Finances
//
//  Created by ALUNO on 11/05/22.
//

import Foundation

struct GastosModel{
    internal init( gasto: Double, tipo:String){
        self.gasto = gasto
        self.tipo = tipo
    }
    
    var gasto:Double
    var tipo:String
    
}
