//
//  ContentViewModel.swift
//  G`s Finances
//
//  Created by ALUNO on 04/05/22.
//

import Foundation

class ContentViewModel:ObservableObject{
    @Published var nome:String =  ""
    var sqlite = SQLiteDatabase()
    var gastosBoard : [GastosModel]
    var ganhosBoard : [GanhosModel]
    
    
    init(){
        gastosBoard = sqlite.getGastos()
        ganhosBoard = sqlite.getGanhos()
    }
    
    func insertNewGanho(ganho:GanhosModel){
        _ = sqlite.insertGanhos(ganhos: ganho)
        ganhosBoard = sqlite.getGanhos()
    }
    
    func insertNewGanho(gasto:GastosModel){
        _ = sqlite.insertGastos(gastos: gasto)
        gastosBoard = sqlite.getGastos()
    }
}
