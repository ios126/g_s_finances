//
//  GastoItemRow.swift
//  G`s Finances
//
//  Created by ALUNO on 18/05/22.
//

import SwiftUI

struct GanhoItemRow: View {
    var position: Int
    var ganhoModel:GanhosModel
    var body: some View {
        HStack{
            Text("\(position)")
            Spacer()
            Text("R$ \(ganhoModel.ganho, specifier: "%.2f")")
            Spacer()
        }
    }
}

struct GanhoItemRow_Previews: PreviewProvider {
    static var previews: some View {
        GanhoItemRow(position: 1, ganhoModel:GanhosModel(ganho:0.0)).previewLayout(.sizeThatFits)
    }
}
